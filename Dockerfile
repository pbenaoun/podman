FROM quay.io/podman/stable:latest
RUN echo "unqualified-search-registries = [\"docker.io\"]" > /etc/containers/registries.conf && \
    echo "alias docker='podman'" >> /root/.bashrc && \
    echo "alias docker='podman'" >> /home/podman/.bashrc
